var app = angular.module('projekt');

app.controller("NemojController", function($rootScope, $scope, $http){

	var self = this;
  	this.username = "prazno";
	this.send = function(name, desc, lore){

		self.username = $rootScope.username;
		self.name = name;
		self.desc = desc;
		self.lore = lore;

		var data = {
			username: $rootScope.username,
			name: name,
			desc: desc,
			lore: lore
		};
		$http({
			data: data,
			method: "POST",
			url: "/send"
		}).then(function successCallback(response){
			$rootScope.registerSuccess = true;
		}), function errorCallback(response){
			console.log("GRESKA");
			$rootScope.registerSuccess = false;
		}

	}
});
