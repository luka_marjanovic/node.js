var app = angular.module('projekt');

app.controller('RegisterController', function($rootScope, $scope, $http){

  console.log("registration");
  var self = this
  this.username = "prazno";
  this.password = "prazno";
  $rootScope.registerSuccess = false;


  this.send = function(username, password){
    self.username = username;
    self.password = password;

    var data = {username: username, password: password};
    $http({
      data: data,
      method: 'POST',
      url: '/register'
    }).then(function successCallback(response){
      console.log("response", response);
      if(response.data.success == false) {
        $rootScope.registerSuccess = false;
      }else{
        $rootScope.registerSuccess = true;
      }
    }, function errorCallback(response){
      console.log("GRESKA");
      $rootScope.registerSuccess = false;
    });
  };
});