var express = module.require('express');
var app = express();
var bodyParser = require('body-parser');
var jwt = require('jsonwebtoken');
var mysql = require('mysql');

app.use(bodyParser.json())
app.use(bodyParser.urlencoded({extended:true}));
app.use(express.static(__dirname + '/static'));


var secret = "neka-fraza";
var users=[];

var connection = mysql.createConnection({
  host: 'localhost',
  user: 'root',
  password: '',
  database: 'Witcher'
});

connection.connect(function(error){
  if(error){
    console.log("Error connectin to db", error);
  } else {
    console.log("Succesfully connected to db !!!");
  }

});

var q = "SELECT * FROM users;";
connection.query(q, function(error, result){
  if(error){
    console.log("MySQL query error", error);
  } else {
    console.log("users: ", result);
    users = result;
  }
});

//connection.end();



app.listen(3001, function(){
  console.log("listening on 3001");
});

app.get('/', function(req, res){
  //response.send(request);
  res.sendFile(__dirname + "/static/views/index.html");
});
///////////////////////////////

app.post("/send", function (req, res){
  var username = req.body.username;
  var name = req.body.name;
  var desc = req.body.desc;
  var lore = req.body.lore;
  var success = false;

  var user = {"username": username, "Name":name, "Description":desc, "Lore":lore};
    updateUser(username,name,desc,lore);
    success = true;
    console.log("user", user);
    res.json({"success": success});
    

    connection.query('UPDATE users SET Name= ?, Description= ?, Lore= ? WHERE Username= ?  ', 
      [name, desc, lore, username] , function(err, result){
      if (err) {
        console.log("Mysql query error", err);
      } else {
        console.log("Users:", result);
      }
    });
});

app.post("/auth", function (req, res){
  var status = 401;
  var response = {"success": false};
  
  var login = {
    username:req.body.username,
    password:req.body.password
  }

  console.log(login);
  var user = getUser(login.username, login.password);

  if (user != null) {
    var token = jwt.sign(user, secret);
    response.success = true;
    response.token = token;
    status = 200;
    res.status(status).json(response);
  } else {
    res.json(response);
  }
});

app.post("/register", function (req, res){
  var username = req.body.username;
  var password = req.body.password;
  var success = false;

  if (!userExists(username)) {
    var user = {"username": username, "password": password};
    users.push(user);
    success = true;
    console.log("user", user);
    res.json({"success": success});
    
    connection.query('INSERT INTO users SET?', user , function(err, result){
      if (err) {
        console.log("Mysql query error", err);
      } else {
        console.log("Users:", result);
      }
    });
  } else {
    res.json({"success": success});
  }
});

app.get("/login", function (req, res){
  res.sendFile(__dirname + "/login.html");
});

app.get("/register", function (req, res){
  res.sendFile(__dirname + "/register.html");
});

///////////////////////////////

var apiRoute = express.Router();

apiRoute.use(function (req, res, next) {
  var token = req.query.token || req.headers["X-Auth-Token"];
  console.log(req.headers);
  if (token) {
    jwt.verify(token, secret, function(err, payload) {
      if (err) {
        return res.status(401).json({success: false, message: "Krivi token"});
      } else {
        next();
      }
    });
  } else {
    return res.status(401).json({success: false, message: "Fali token"});
  }
});

apiRoute.get("/users", function (req, res) {
  res.status(200).json(users);
});

app.use("/api", apiRoute);

//--------------------------------------------------------------------

function getUser(username, password) {
  for(var i=0; i<users.length; i++){
    if(users[i].username == username && users[i].password == password){
      return users[i];
    }
  }
  return null;
}

function verifyLogin(username, password){
  for(var i=0; i<users.length; i++){
    if(users[i].username == username && users[i].password == password){
      return true;
    }
  }
  return false;
}

function userExists(username) {
  for(var i=0; i<users.length; i++){
    if(users[i].username==username){
      return true;
    }
  }
  return false;
}

function updateUser(username,name,desc,lore,timestamp){
  for (var i = 0; i < users.length; i++) {
    if(users[i].username == username) {
      users[i].name = name;
      users[i].desc = desc;
      users[i].lore = lore;
    }
  }
}